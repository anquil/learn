import java.util.Scanner;

public class IfDemo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int i = 0 ;
        if (sc.hasNext()) {
             i = sc.nextInt();
        }
        if (i < 5 ) {
            System.out.println("Введённое число меньше 5");
        } else {
            System.out.println("Введённое число больше или равно 5");
        }
    }
}
