public class Rectangle {
    int x;
    int y;

    int square() {
        return x * y;
    }

    int perimeter() {
        return (x + y) * 2;
    }

}
