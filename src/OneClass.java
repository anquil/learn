public class OneClass {
    public static void main(String[] args) {
        // создание и инициализация примитивного целочисленного типа
        int i = 0;

        // создание переменной  примитивного целочисленного типа
        int j;
        j = 1;
        j = 2;
        j = 5 + 7;

        // создание и инициализация обектного типа
        Object object = new Object();

        // создание и переменной обектного типа
        Object object2;
        // инициализация переменной обектного типа
        object2 = new Object();
        object2 = new Object();


        Integer integer = new Integer(4);
        integer = 7;


        Rectangle rectangleFirst = new Rectangle();
        rectangleFirst.x = 3;
        rectangleFirst.y = 4;

        Rectangle rectangleTwo = new Rectangle();
        rectangleTwo.x = 10;
        rectangleTwo.y = 20;

        int squareOfRectangle = rectangleFirst.square();

        int squareOfRectangle2 = rectangleTwo.square();

        System.out.println(squareOfRectangle);

        System.out.println(squareOfRectangle2);

        System.out.println("End of program");
    }
}
